import 'jest'
import request from 'supertest'
import express from 'express'
import Server from '../index'
import bodyParser from 'body-parser'
import { Routes } from '../router'
import { connection } from '../config/connectdb'

const app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
Object.keys(Routes).forEach(key => {
    app[Routes[key].verb](Routes[key].endPoint, Routes[key].handler)
})
connection()

describe('Test servicios api flights', () => {
    it('Test servidor start', () => {
        return request(app).get('/').expect(200).then((res) => {
            expect(res.status).toBe(200)
        })
    })
    it('Test GET/ Airports', () => {
        return request(app).get('/getAirports').then((res) => {
            expect(200).toEqual(res.status)
            expect(Array.isArray(res.body)).toBe(true)
        })
    })
    it('Test GET/ findFlights', () => {
        return request(app).get('/flights?destiny=BOG&origin=MDE&date=10-28-2018').then((res) => {
            expect(200).toEqual(res.status)
            expect(Array.isArray(res.body)).toBe(true)
        })
    })
    it('Test GET/ roundTrip', () => {
        return request(app).get('/roundTrip?destiny=BOG&origin=MDE&date=10-28-2018&returnDate=10-30-2018').
            then((res) => {
                expect(200).toEqual(res.status)
                expect(Array.isArray(res.body.going)).toBe(true)
            })
    })
    it('Test POST/ multitrip', () => {
        return request(app).post('/multiTrip').send({
            trips:
                [{ 'destiny': 'MDE', 'origin': 'BOG', 'date': '10-30-18' }]
        }).set('Content-Type', 'application/json')
            .set('Accept', 'application/json').then((res) => {
                expect(200).toEqual(res.status)
                expect(Array.isArray(res.body.Flight1)).toBe(true)
            })
    })
    it('Test POST/ createAirport', () => {
        return request(app).post('/createAirport').send({
            'iata': 'Prueb',
            'name': 'Aeropuerto Internacional Rafael Núñez',
            'city': 'Prueba'
        }).set('Content-Type', 'application/json')
            .set('Accept', 'application/json').then((res) => {
                expect(200).toEqual(res.status)
            })
    })
})