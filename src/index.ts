process.env.NODE_CONFIG_DIR = `${__dirname}/env`
import { StructureApp } from './core'
import { appMiddleware } from './middleware'
import {Routes} from './router'
import {connection} from './config/connectdb'

export default class Server extends StructureApp {
    constructor() {
        super()
        connection()
        this.setMiddlewares(appMiddleware)
        this.setRoutes(Routes)
    }
    public static bootstrap(): Server {
        return new Server
    }
}
Server.bootstrap().startServer()