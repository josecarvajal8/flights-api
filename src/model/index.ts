import { Model, model } from 'mongoose'
import { IFlightModel, FlightSchema } from './Schemas/flights'
import {AirportSchema, IAirportModel} from './Schemas/airports'
export const MFlight: Model<IFlightModel> = model<IFlightModel>('Flights', FlightSchema)
export const MAirport: Model<IAirportModel> = model<IAirportModel> ('Airports', AirportSchema)