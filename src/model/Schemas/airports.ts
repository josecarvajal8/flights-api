import { Document, Schema } from 'mongoose'
import { IAirpot } from '../../interfaces'
export const AirportSchema: Schema = new Schema({
    iata: { type: String, required: true },
    name: { type: String, required: true },
    city: { type: String, required: true }
})
export interface IAirportModel extends IAirpot, Document { }