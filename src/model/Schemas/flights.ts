import { Document, Schema } from 'mongoose'
import { AirportSchema } from './airports'
import { IFlight } from '../../interfaces/'
export const FlightSchema: Schema = new Schema({
    origin: { type: AirportSchema, required: true },
    destination: { type: AirportSchema, required: true },
    image: { type: String },
    segments: { type: Array },
    capacity: { type: Number, required: true },
    duration: { type: Number, required: true },
    departureTime: { type: Date, required: true },
    landingTime: { type: Date, required: true },
    date: { type: Date, required: true },
    price: { type: Number, required: true },
    company: { type: String, required: true }
})
export interface IFlightModel extends IFlight, Document { }