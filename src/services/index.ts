import { MFlight, MAirport } from '../model/'
import { IFlightModel } from '../model/Schemas/flights'
import { AirportSchema, IAirportModel } from '../model/Schemas/airports'
import { IRoundTrip, IOneWayFlight } from '../interfaces/'
export const services = {
    findFlights: async (params: IOneWayFlight) => {
        try {
            const { destiny, origin, date } = params
            const data = await MFlight.find({
                'destination.iata': destiny, 'origin.iata':
                    origin, 'date': new Date(date)
            })
            const res = data.length > 0 ? data : { data: 'No se encontraron vuelos ' }
            return res
        } catch (error) {
            return error
        }
    },
    createFlight: async (newFlight: IFlightModel) => {
        try {
            const data = MFlight.create(Object.assign({}, newFlight))
            return data
        } catch (error) {
            return error
        }
    },
    roundTrip: async (params: IRoundTrip) => {
        try {
            const { destiny, origin, date, returnDate } = params
            const going = await services.findFlights({
                destiny: destiny, origin: origin, date: date
            })
            const returning = await services.findFlights(
                { destiny: origin, origin: destiny, date: returnDate })
            const trip = { going: going, returning: returning }
            return trip
        } catch (error) {
            return error
        }
    },
    multiDestiny: async (params: any) => {
        try {
            const { trips } = params
            let multiFlights: any = {}
            for (let i = 0; i < trips.length; i++) {
                multiFlights[`Flight${i + 1}`] = await services.findFlights(trips[i])
            }
            return multiFlights
        } catch (error) {
            return error
        }
    },
    createAirports: async (params: IAirportModel) => {
        try {
            const data = await MAirport.create(Object.assign({}, params))
            return data
        } catch (error) {
            return error
        }
    },
    getAirports: async () => {
        try {
            const data = await MAirport.find()
            return data
        } catch (error) {
            return error
        }
    }
}