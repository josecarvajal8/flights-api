import config from 'config'
import express from 'express'
import http from 'http'

export class StructureApp {
    public app: any
    private server: any
    constructor() {
        this.app = express()
    }
    public setMiddlewares(middlewares: any): void {
        Object.keys(middlewares).forEach(key => {
            this.app.use(middlewares[key].handler)
        })
    }
    public setRoutes(router: any): void {
        Object.keys(router).forEach(key => {
            this.app[router[key].verb](router[key].endPoint, router[key].handler)
        })
    }
    public startServer() {
        const conf = config.get('server')
        const portNumber: number = (conf as any).port
        this.server = http.createServer(this.app)
        this.server.listen(portNumber, () => {
            console.log(`Running at http://localhost:${portNumber}/`)
        })
    }
}