import { Request, Response } from 'express'
import { services } from '../services'

export const Controller = {
    home: (req: Request, res: Response) => {
        res.send('Server running')
    },
    createFlight: async (req: Request, res: Response) => {
        const dataToSend = await services.createFlight(req.body)
        res.send(dataToSend)
    },
    getFlights: async (req: Request, res: Response) => {
        const dataTosend = await services.findFlights(req.query)
        res.send(dataTosend)
    },
    roundTrip: async (req: Request, res: Response) => {
        const dataTosend = await services.roundTrip(req.query)
        res.send(dataTosend)
    },
    multiDestiny: async (req: Request, res: Response) => {
        const dataToSend = await services.multiDestiny(req.body)
        res.send(dataToSend)
    },
    createAirports: async (req: Request, res: Response) => {
        const dataToSend = await services.createAirports(req.body)
        res.send(dataToSend)
    },
    getAirports: async (req: Request, res: Response) => {
        const dataToSend = await services.getAirports()
        res.send(dataToSend)
    }
}