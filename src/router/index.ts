import { Controller } from '../controller'
export const Routes: { [index: string]: any } = {
    index: {
        verb: 'get',
        endPoint: '/',
        handler: Controller.home
    },
    createFlight: {
        verb: 'post',
        endPoint: '/newFlight',
        handler: Controller.createFlight
    },
    getFlights: {
        verb: 'get',
        endPoint: '/flights',
        handler: Controller.getFlights
    },
    roundTrip: {
        verb: 'get',
        endPoint: '/roundTrip',
        handler: Controller.roundTrip
    },
    multiDestiny: {
        verb: 'post',
        endPoint: '/multiTrip',
        handler: Controller.multiDestiny
    },
    createAirport: {
        verb: 'post',
        endPoint: '/createAirport',
        handler: Controller.createAirports
    },
    getAirpots: {
        verb: 'get',
        endPoint: '/getAirports',
        handler: Controller.getAirports
    }
}