export interface IAirpot {
    iata: string
    name: string,
    city: string
}
export interface IFlight {
    origin: IAirpot
    destination: IAirpot
    image?: string
    segments?: any
    capacity: number
    duration: number
    departureTime: Date,
    landingTime: Date,
    price: number,
    company: string
    date: Date
}
export interface IOneWayFlight {
    destiny: string
    origin: string
    date: string
}
export interface IRoundTrip {
    destiny: string
    origin: string
    date: string
    returnDate: string
}