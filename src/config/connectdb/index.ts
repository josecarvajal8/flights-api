import mongoose from 'mongoose'
const dataDb = 'mongodb://root:root@ds129434.mlab.com:29434/hotels-app'
mongoose.connect(dataDb)
const conn = mongoose.connection
export const connection = () => {
    conn.on('error', console.error.bind(console, 'connection error: '))
    conn.once('open', () => console.log('conexion exitosa'))
}