import cors from 'cors'
import bodyParser from 'body-parser'
export const appMiddleware: { [index: string]: any } = {
    cors: {
        handler: cors()
    },
    urlencodedParser: {
        handler: bodyParser.urlencoded({ extended: false })
    },
    jsonParser: {
        handler: bodyParser.json()
    }
}