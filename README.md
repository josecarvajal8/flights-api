# Api Flights

Proyecto realizado con el fin de consumir servicios, que retornen vuelos dependiendo de la busqueda (Solo ida, vuelo redondo, multi-destino), la api hace
uso de Express e implementa 6 endpoints. La api fue realizada para cumplir el reto propuesto por Almundo.com



# Requisitos

- Poseer el entorno de desarrollo Node
- Poseer git
- Clonar el repositorio.

# Instalación

- Clonar el repositorio.
- Desde una terminal de la maquina, navegar entre carpetas hasta llegar a la del repositorio clonado.
- Ejecutar el comando:

> npm install

Luego de ejecutar el comando, se instalarán todas las dependencias del proyecto.

# Ejecución

Para ejecutar la aplicación puede ejecutar el siguiente comando:

> npm run dev

Para ejecutar los test de las implementados de la rutas ejecute:

> npm run test

# Dependencias implementadas
-body-parser
-config
-cors
-eslint
-express
-gulp
-jest
-mongoose
-supertest
-ts-node
-tslint
-typescript

#Uso de los servicios

Desde su maquina puede ingresar a: http://localhost:6080/
Tambien puede hacer uso de las siguientes rutas:
- /newFlight (POST)
- /flights (GET) -- queryparams
- /roundTrip (GET) -- queryparams
- /multiTrip (POST)
- /createAirport (POST)
- /getAirports (GET)

# Versión

1.0.0

# Autor

Jose Manuel Carvajal




